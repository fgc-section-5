use std

use quiver

use "types"
use "kc"
use "roots"

pkg =
        generic vname : (i : @a, b : @b -> byte[:]) :: numeric @a, numeric @b
        generic wname : (i : @a, b : @b -> byte[:]) :: numeric @a, numeric @b
        const rename_and_move : (q1 : quiver.quiver#, q2 : quiver.quiver#, from : byte[:], to : byte[:] -> std.result(void, byte[:]))

        /*
           This is what's used for verify_murot, and what should be used
           in the actual section 5 writeup.
         */
        const rename_according_to_rot : (kc : KC_type, q1 : quiver.quiver#, q2 : quiver.quiver# -> std.result(void, byte[:]))

        /*
           This is what's used during the construction. It ignores some
           of the sigmaG rotations. Some.
         */
        const rename_according_to_twistingrot : (kc : KC_type, q1 : quiver.quiver#, q2 : quiver.quiver# -> std.result(void, byte[:]))
        const rename_according_to_twistingrotinv : (kc : KC_type, q1 : quiver.quiver#, q2 : quiver.quiver# -> std.result(void, byte[:]))

        const rename_according_to_flip : (kc : KC_type, q1 : quiver.quiver#, q2 : quiver.quiver# -> std.result(void, byte[:]))
        const rename_according_to_twistingflip : (kc : KC_type, q1 : quiver.quiver#, q2 : quiver.quiver# -> std.result(void, byte[:]))

        const get_node_xshifts : (kc : KC_type -> std.htab(weyl_reflection, int64)#)
;;

var node_xshifts_stored : std.htab(KC_type, std.htab(weyl_reflection, int64)#)#

const __init__ = {
        node_xshifts_stored = std.mkht()
}

const __finit__ = {
        for (key, val) : std.byhtkeyvals(node_xshifts_stored)
                std.htfree(val)
        ;;
        std.htfree(node_xshifts_stored)
}

/* I keep bikeshedding this damn thing */
generic vname = {i, j
        /*
           i : "y" position (simple root within G system)
           j : "x" position (simpl root within A_l system)
         */
        -> std.fmt("v{x};{x}", i, j)
}

generic wname = {i, j
        -> std.fmt("w{x};{x}", i, j)
}

/*
   Renames and moves a vertex. Specifically: assume that q2 is a
   duplicate of q1. Take the vertex named "from" in q1. Now take the
   corresponding vertex in q2 (i.e., the one with the same index as
   "from" has in q1). Call it "to", and move it to the x,y position that
   "to" occupies in q1.
 */
const rename_and_move = {q1 : quiver.quiver#, q2 : quiver.quiver#, from : byte[:], to : byte[:]
        match quiver.find_vertex(q1, from)
        | `std.None: -> `std.Err std.fmt("cannot rename {} to {}; {} does not exist in q1", from, to, from)
        | `std.Some (fromj, fromp):
                match quiver.find_vertex(q1, to)
                | `std.None: -> `std.Err std.fmt("cannot rename {} to {}; {} does not exist in q1", from, to, to)
                | `std.Some (toj, top):
                        std.slfree(q2.v[fromj].name)
                        q2.v[fromj].name = std.sldup(to)
                        q2.v[fromj].x_pos = top.x_pos
                        q2.v[fromj].y_pos = top.y_pos
                ;;
        ;;

        -> `std.Ok void
}

/*
   Given q_pre, and q_post, which is just q_pre after applying murot,
   rename vertices of q_post so that q_post becomes graph-isomorphic to
   q_pre.
 */
const rename_according_to_rot = {kc, q_pre, q_post
        var c
        match get_coxeter_elt(kc)
        | `std.Ok cc: c = cc
        | `std.Err e: -> `std.Err std.fmt("get_coxeter_elt({}): {}", kc, e)
        ;;

        var sigmaG
        match get_sigmaG_permutation(kc)
        | `std.Ok sigmaGG: sigmaG = sigmaGG
        | `std.Err e: -> `std.Err std.fmt("get_sigmaG_permutation({}): {}", kc, e)
        ;;

        var l = coxeter_num(kc) / 2 - 1

        for i : c
                var Ai = std.fmt("A{}", i)
                var Bi = std.fmt("B{}", i)
                var Ci = std.fmt("C{}", i)

                var AsigmaGi = std.fmt("A{}", sigmaG[i])
                var BsigmaGi = std.fmt("B{}", sigmaG[i])
                var CsigmaGi = std.fmt("C{}", sigmaG[i])

                match rename_and_move(q_pre, q_post, Bi, Ci)
                | `std.Err e: -> `std.Err std.fmt("rearranging Q' failed: {}", e)
                | `std.Ok void:
                ;;

                match rename_and_move(q_pre, q_post, Ci, AsigmaGi)
                | `std.Err e: -> `std.Err std.fmt("rearranging Q' failed: {}", e)
                | `std.Ok void:
                ;;

                match rename_and_move(q_pre, q_post, Ai, Bi)
                | `std.Err e: -> `std.Err std.fmt("rearranging Q' failed: {}", e)
                | `std.Ok void:
                ;;

                std.slfree(Ai)
                std.slfree(Bi)
                std.slfree(Ci)
                std.slfree(AsigmaGi)
                std.slfree(BsigmaGi)
                std.slfree(CsigmaGi)
        ;;

        -> `std.Ok void
}

/*
   murot does not quite perform a 120 degree rotation: there's an
   unfortunate permutation of vertices by sigma_G. When actually
   calculating coordinates, we have to perform the rename, undo the
   monomial map, and then permute by sigma_G back in the seed torus (or
   something like that).

   For the purposes of constructing the quiver, however, we don't have
   variables yet: we simply operate on a graph level. Therefore, we
   combine both renamings into one step, and we do that step entirely on
   the graph.
 */
const rename_according_to_twistingrot = {kc, q_pre, q_post
        var c
        match get_coxeter_elt(kc)
        | `std.Ok cc: c = cc
        | `std.Err e: -> `std.Err std.fmt("get_coxeter_elt({}): {}", kc, e)
        ;;

        var sigmaG
        match get_sigmaG_permutation(kc)
        | `std.Ok sigmaGG: sigmaG = sigmaGG
        | `std.Err e: -> `std.Err std.fmt("get_sigmaG_permutation({}): {}", kc, e)
        ;;

        var l = coxeter_num(kc) / 2 - 1

        for i : c
                for var j = 1; j <= l; ++j
                        var name_in_pre : byte[:] = vname(i,j)
                        var name_in_post : byte[:] = vname(sigmaG[i], l - j + 1)

                        match rename_and_move(q_pre, q_post, name_in_pre, name_in_post)
                        | `std.Err e: -> `std.Err std.fmt("rearranging Q' failed: {}", e)
                        | `std.Ok void:
                        ;;

                        std.slfree(name_in_pre)
                        std.slfree(name_in_post)
                ;;

                var Ai = std.fmt("A{}", i)
                var Bi = std.fmt("B{}", i)
                var Ci = std.fmt("C{}", i)

                var AsigmaGi = std.fmt("A{}", sigmaG[i])
                var BsigmaGi = std.fmt("B{}", sigmaG[i])
                var CsigmaGi = std.fmt("C{}", sigmaG[i])

                match rename_and_move(q_pre, q_post, Bi, CsigmaGi)
                | `std.Err e: -> `std.Err std.fmt("rearranging Q' failed: {}", e)
                | `std.Ok void:
                ;;

                match rename_and_move(q_pre, q_post, Ci, AsigmaGi)
                | `std.Err e: -> `std.Err std.fmt("rearranging Q' failed: {}", e)
                | `std.Ok void:
                ;;

                match rename_and_move(q_pre, q_post, Ai, Bi)
                | `std.Err e: -> `std.Err std.fmt("rearranging Q' failed: {}", e)
                | `std.Ok void:
                ;;
        ;;

        -> `std.Ok void
}

const rename_according_to_twistingrotinv = {kc, q_pre, q_post
        var c
        match get_coxeter_elt(kc)
        | `std.Ok cc: c = cc
        | `std.Err e: -> `std.Err std.fmt("get_coxeter_elt({}): {}", kc, e)
        ;;

        var sigmaG
        match get_sigmaG_permutation(kc)
        | `std.Ok sigmaGG: sigmaG = sigmaGG
        | `std.Err e: -> `std.Err std.fmt("get_sigmaG_permutation({}): {}", kc, e)
        ;;

        var l = coxeter_num(kc) / 2 - 1

        for i : c
                for var j = 1; j <= l; ++j
                        var name_in_pre : byte[:] = vname(i,j)
                        var name_in_post : byte[:] = vname(sigmaG[i], l - j + 1)

                        match rename_and_move(q_pre, q_post, name_in_post, name_in_pre)
                        | `std.Err e: -> `std.Err std.fmt("rearranging Q' failed: {}", e)
                        | `std.Ok void:
                        ;;

                        std.slfree(name_in_pre)
                        std.slfree(name_in_post)
                ;;

                var Ai = std.fmt("A{}", i)
                var Bi = std.fmt("B{}", i)
                var Ci = std.fmt("C{}", i)

                var AsigmaGi = std.fmt("A{}", sigmaG[i])
                var BsigmaGi = std.fmt("B{}", sigmaG[i])
                var CsigmaGi = std.fmt("C{}", sigmaG[i])

                match rename_and_move(q_pre, q_post, Bi, Ai)
                | `std.Err e: -> `std.Err std.fmt("rearranging Q' failed: {}", e)
                | `std.Ok void:
                ;;

                match rename_and_move(q_pre, q_post, CsigmaGi, Bi)
                | `std.Err e: -> `std.Err std.fmt("rearranging Q' failed: {}", e)
                | `std.Ok void:
                ;;

                match rename_and_move(q_pre, q_post, AsigmaGi, Ci)
                | `std.Err e: -> `std.Err std.fmt("rearranging Q' failed: {}", e)
                | `std.Ok void:
                ;;
        ;;

        -> `std.Ok void
}

/* As rename_according_to_rot, but for flip */
const rename_according_to_flip = {kc, q_pre, q_post
        -> `std.Ok void
}

const rename_according_to_twistingflip = {kc, q_pre, q_post
        var c
        match get_coxeter_elt(kc)
        | `std.Ok cc: c = cc
        | `std.Err e: -> `std.Err std.fmt("get_coxeter_elt({}): {}", kc, e)
        ;;

        var sigmaG
        match get_sigmaG_permutation(kc)
        | `std.Ok perm: sigmaG = perm
        | `std.Err e: -> `std.Err std.fmt("get_sigmaG_permutation({}): {}", kc, e)
        ;;

        var l = coxeter_num(kc) / 2 - 1
        var L = 2 * l + 1

        for i : c
                for var j = 1; j <= L; ++j
                        var name_in_pre : byte[:] = wname(i,j)
                        var iprime = i
                        var jprime = j
                        if j <= l
                                jprime += l + 1
                        elif j > l + 1
                                jprime -= l + 1
                        ;;
                        
                        var name_in_post : byte[:] = wname(iprime, jprime)

                        match rename_and_move(q_pre, q_post, name_in_pre, name_in_post)
                        | `std.Err e: -> `std.Err std.fmt("rearranging Q' failed: {}", e)
                        | `std.Ok void:
                        ;;

                        std.slfree(name_in_pre)
                        std.slfree(name_in_post)
                ;;
        ;;

        -> `std.Ok void
}

const get_node_xshifts = {kc
        match std.htget(node_xshifts_stored, kc)
        | `std.Some stored: -> stored
        | `std.None:
        ;;

        var node_xshifts : std.htab(weyl_reflection, int64)# = std.mkht()
        var dynkin_edges : (weyl_reflection, weyl_reflection)[:] = get_edges(kc)
        var T = get_tree_partition(kc)
        var c = [][:]
        match get_coxeter_elt(kc)
        | `std.Ok cc: c = cc
        | `std.Err e: goto done
        ;;

        /* Space out the roots of the tree equally. */
        for var j = 0; j < T[1].len; ++j
                std.htput(node_xshifts, T[1][j], j * 25)
        ;;

        /*
           For each node, determine how many children it has, and space
           out the siblings accordingly.
         */
        for var i = 1; i < T.len; ++i
                for si : T[i]
                        var this_xshift = std.htgetv(node_xshifts, si, 0)
                        var children = [][:]
                        for (a, b) : dynkin_edges
                                if a == si
                                        std.slpush(&children, b)
                                ;;
                        ;;

                        var leftmost_child_x = this_xshift - (50 * (children.len - 1)) / 2
                        for var j = 0; j < children.len; ++j
                                std.htput(node_xshifts, children[j], leftmost_child_x + 50 * j)
                        ;;
                        std.slfree(children)
                ;;
        ;;

:done
        std.htput(node_xshifts_stored, kc, node_xshifts)

        -> node_xshifts
}
