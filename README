This is intended to be a readable implementation for section 5 of
https://arxiv.org/abs/2104.04941 , and to obtain numerical evidence.

You can construct Q for particular G, output the mutations defined by
section 5, and numerically verify that those mutations work. Perhaps
more relevantly, you can translate this implementation into whatever
system you'd prefer to actually work with.

Use it like

    s5 -g <group type> <command> [ -S <RNG seed> ] [ -f ]

where command is one of

    clav-quiver
    latex-quiver

    print-M

    print-murot
    print-muflip
    // print-muswap

    test-murot
    test-muflip
    // test-muswap

(muswap is currently unimplemented, because verifying it requires
transforming factorization coordinates between i and i^-1, which is
hard.)

The ‘-f’ flag is for “testing/printing Fast mutations”. It turns out
that much of the mutations for rotations and flips deal with permuting
vertices of the quiver, without applying any “interesting” rational
maps. If you are willing to incorporate these permutations into the
assignment maps between seed tori and Conf_k^*, then the mutations may
be shortened. This particular code does not benefit time-wise
significantly from this reduction, but you might find differently.

Examples:

    # Create the quiver for F4, and write it to QF4.clav
    s5 -g F4 clav-quiver > QF4.clav

    # Show the map M between Conf_3(G/N) and T_Q for E6
    s5 -g E6 print-M

    # Output murot for G2
    s5 -g G2 print-murot

    # Numerically check murot for B2, using seed 1 for the RNG
    s5 -g B2 test-murot -S 1

    # Numerically check muflip for E8, using the short mutation
    s5 -g E8 test-muflip -f
