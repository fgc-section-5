use std

use quiver
use yakmo

pkg =
        const latex_from_quiver : (q : quiver.quiver# -> byte[:])
;;

const latex_from_quiver = {q
        var sb : std.strbuf# = std.mksb()
        std.sbfmt(sb, "\\documentclass{{standalone}}\n")
        std.sbfmt(sb, "\n")
        std.sbfmt(sb, "\\usepackage{{tikz}}\n")
        std.sbfmt(sb, "\\usepackage[outline]{{contour}}\n")
        std.sbfmt(sb, "\\contourlength{{1.2pt}}\n")
        std.sbfmt(sb, "\n")
        std.sbfmt(sb, "\\begin{{document}}\n")
        std.sbfmt(sb, "  %noformat{{\n")
        std.sbfmt(sb, "  \\begin{{tikzpicture}}\n")
        std.sbfmt(sb, "    \\tikzstyle{{v1}} = [circle, outer xsep=0.5pt, fill, inner sep=1.5pt]\n")
        std.sbfmt(sb, "    \\tikzstyle{{v2}} = [circle, outer xsep=0.5pt, fill, inner sep=3.0pt]\n")
        std.sbfmt(sb, "    \\tikzstyle{{v3}} = [circle, outer xsep=0.5pt, fill, inner sep=4.5pt]\n")
        std.sbfmt(sb, "    \\tikzstyle{{unf}} = [color=darkgray]\n")
        std.sbfmt(sb, "    \\tikzstyle{{frz}} = [color=blue!60!darkgray!70]\n")
        std.sbfmt(sb, "    \\tikzstyle{{e}} = [->, >={{stealth}}, preaction={{draw=white, ->, >={{stealth}}, shorten >=-1.5pt, ultra thick}}]\n")
        std.sbfmt(sb, "    \\tikzstyle{{h}} = [dashed, color=gray, ->, >={{stealth}}]\n")
        std.sbfmt(sb, "    \\tikzstyle{{r}} = [->, color=red!70!darkgray!70, >={{stealth}}]\n")
        std.sbfmt(sb, "\n")
        std.sbfmt(sb, "    \\tikzset{{font=\\tiny}}\n")
        std.sbfmt(sb, "\n")
        for var j = 0; j < q.v.len; ++j
                var v : quiver.vertex# = &q.v[j]
                var tikzname : byte[:] = tikzname_from_v(v)
                var x : byte[:] = x_from_v(v)
                var y : byte[:] = y_from_v(v)
                std.sbfmt(sb, "    \\coordinate[v{}] ({}) at ({},{}) {{}};\n", v.d, tikzname, x, y)
                std.slfree(tikzname)
                std.slfree(x)
                std.slfree(y)
        ;;

        std.sbfmt(sb, "\n")

        for var j = 0; j < q.v.len; ++j
                for var k = 0; k < q.v.len; ++k
                        var s : yakmo.Q#
                        match quiver.get_sigma_ii(q, j, k)
                        | `std.None: continue
                        | `std.Some ss:
                                s = ss
                        ;;
                        auto s

                        match yakmo.cmp_zero(s)
                        | `std.After:
                        | _: continue
                        ;;

                        var jname = tikzname_from_v(&q.v[j])
                        var kname = tikzname_from_v(&q.v[k])
                        if yakmo.eqQ(s, 1, 2)
                                std.sbfmt(sb, "    \\draw[h] ({}) -- ({});\n", jname, kname)
                        elif yakmo.eqQ(s, 1, 1)
                                std.sbfmt(sb, "    \\draw[e] ({}) -- ({});\n", jname, kname)
                        else
                                std.sbfmt(sb, "    \\draw[r] ({}) -- ({}) node[midway, above] {{${}$}};\n", jname, kname, s)
                        ;;
                        std.slfree(jname)
                        std.slfree(kname)
                ;;
        ;;

        std.sbfmt(sb, "\n")

        for var j = 0; j < q.v.len; ++j
                var v : quiver.vertex# = &q.v[j]
                var angle : byte[:] = angle_from_v(v)
                var label : byte[:] = label_from_v(v)
                var color : byte[:] = color_from_v(v)
                var tikzname : byte[:] = tikzname_from_v(v)
                std.sbfmt(sb, "    \\node[v{},label={{{}:{{\\contour{{white}}{{${}$}}}}}}, {}] (n_{}) at ({}) {{}};\n", v.d, angle, label, color, tikzname, tikzname)
                std.slfree(angle)
                std.slfree(label)
                std.slfree(color)
                std.slfree(tikzname)
        ;;
        std.sbfmt(sb, "  \\end{{tikzpicture}}\n")
        std.sbfmt(sb, "  %}}noformat\n")
        std.sbfmt(sb, "\\end{{document}}\n")

        -> std.sbfin(sb)
}

const angle_from_v = {v
        var c : char = (v.name[0] : char)
        match c
        | 'A' : -> std.sldup("270")
        | 'B' : -> std.sldup("180")
        | 'C' : -> std.sldup("0")
        | _   : -> std.sldup("[label distance=-3pt]95")
        ;;
}

const label_from_v = {v
        var sb : std.strbuf# = std.mksb()
        var seen_digit = false
        for c : std.bychar(v.name)
                if std.isalpha(c) || std.isdigit(c)
                        if std.isdigit(c) && !seen_digit
                                std.sbputs(sb, "_{")
                                seen_digit = true
                        ;;

                        std.sbputc(sb, c)
                ;;
        ;;

        if seen_digit
                std.sbputs(sb, "}")
        ;;

        -> std.sbfin(sb)
}

const color_from_v = {v
        match v.color
        | (0x4c, 0xe7, 0xff): -> std.sldup("frz")
        | _                 : -> std.sldup("unf")
        ;;
}

const tikzname_from_v = {v
        var sb : std.strbuf# = std.mksb()
        for c : std.bychar(v.name)
                if std.isalpha(c) || std.isdigit(c)
                        std.sbputc(sb, c)
                ;;
        ;;

        -> std.sbfin(sb)
}

const x_from_v = {v
        /* For things like E8, use a smaller multiple, like 0.9 */
        -> std.fmt("{}", 1.25 * (v.x_pos : flt64) / 100.0)
}

const y_from_v = {v
        -> std.fmt("{}", -1.0 * (v.y_pos : flt64) / 75.0)
}
